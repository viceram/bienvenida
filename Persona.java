public class Persona
{
	private String nombre;
        private int documento;

        public void setNombre(String nombre){
        	this.nombre=nombre;
        }
        public String getNombre(){
        	return nombre;
        }

        public void setDocumento(int documento){
        	this.documento=documento; 
        }
        public int getDocumento(){
		return documento;
        }

        public String toString(){
		return "Nombre: " + nombre + " Documento: " + documento;
	}
}