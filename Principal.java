import saludo.Saludo;

public class Principal {
    public static void main(String[] args) {
        Saludo saludo1 = new Saludo();
        saludo1.setSaludoPrincipal("Hola ");

        Saludo saludo2 = new Saludo();
        saludo2.setSaludoPrincipal("Hello ");

        saludo1.saludoDeMadrugada();
        saludo2.saludoEnLaTarde();
        saludo1.saludoEnLaNoche();
        
        Persona persona1 = new Persona();
        persona1.setNombre("Diego");
        persona1.setDocumento(11111111);
        
        System.out.println("Estado de persona1: Nombre " + persona1.getNombre() + " Documento " + persona1.getDocumento());
        System.out.println("Estado del objeto: " + persona1.toString());
        System.out.println("Estado del objeto: " + saludo1.getSaludoPrincipal());
    }
    
}