package saludo;

public class Saludo
{
    private String saludoPrincipal;   
 
    public void saludoDeMadrugada()
    {
        System.out.println(saludoPrincipal + "buenos días");
    }
    
    public void saludoEnLaTarde()
    {
        System.out.println(saludoPrincipal + "buenas tardes");
    }

    public void saludoEnLaNoche()
    {
        System.out.println(saludoPrincipal + "buenas noches");
    }

    //getter y setter
    public void setSaludoPrincipal(String saludoPrincipal)
    {
         this.saludoPrincipal=saludoPrincipal;
    }

    public String getSaludoPrincipal()
    {
         return saludoPrincipal;
    }

}